# Rust coverage

Example rust project with code coverage and testing report

### Coverage in GitLab pages

Coverage html report [url](https://gitlab.com/bogdanoval/rust-coverage/-/jobs/artifacts/main/file/coverage/index.html?job=cargo-coverage)

Cargo Doc for this project [url](https://bogdanoval.gitlab.io/rust-coverage)
