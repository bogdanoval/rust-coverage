use tracing::{info, Level};
use tracing_subscriber::EnvFilter;
use rust_coverage::add;

fn main() {
    tracing_subscriber::fmt()
        .with_ansi(false)
        .with_env_filter(
            EnvFilter::from_default_env()
                .add_directive(Level::INFO.into())
                .add_directive(format!("{}=debug", module_path!()).parse().unwrap()),
        )
        .init();
    info!("Hello CI Coverage");
    let sum = add(5, 5);
    info!("5 + 5 = {}", sum);
}
